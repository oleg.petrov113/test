<?php

declare(strict_types=1);

namespace Core\Internals\Struct;


/**
 * Структурный трейт для реализации паттерна singleton
 *
 * @see https://refactoring.guru/ru/design-patterns/singleton
 *
 * @package Core\Internals\Struct
 */
trait InstanceTrait
{
    /**
     * @var array Хранилище экземпляров объектов
     */
    protected static $instances = [];

    /**
     * Возвращает экземпляр текущего объекта
     *
     * @return static
     */
    final public static function instance()
    {
        $instance =& static::$instances[static::class];

        if (null === $instance) {
            $instance = new static;
        }

        return $instance;
    }

    /**
     *
     */
    final private function __construct()
    {
        $this->init();
    }

    /**
     * Инициализация
     *
     * @return void
     */
    protected function init(): void
    {
    }

    /**
     * @return void
     */
    final private function __wakeup()
    {
        throw new \LogicException(
            sprintf("Невозможно десериализовать %s", static::class)
        );
    }

    /**
     * @return void
     */
    final private function __clone()
    {
    }
}