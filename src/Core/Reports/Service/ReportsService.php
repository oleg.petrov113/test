<?php

declare(strict_types = 1);

namespace Core\Reports\Service;

use Bitrix\Main\ORM\Objectify\Collection;
use Core\Reports\Repository\ReportsRepository;
use Core\Internals\Struct\InstanceTrait;

class ReportsService
{
    use InstanceTrait;

    private ReportsRepository $reportsRepository;

    public function __construct() {
        $this->reportsRepository = ReportsRepository::instance();
    }

    /**
     * @return array|null
     */
    public function getReports(): ?array
    {
        [$elements, $sections] = [$this->reportsRepository->getElements(), $this->reportsRepository->getSections()];

        return $this->resolveReports($elements, $sections);
    }

    /**
     * @param Collection|null $elements
     * @param array|null $sections
     *
     * @return array|null
     */
    private function resolveReports(?Collection $elements, ?array $sections): ?array
    {
        if (!$elements || !$sections) {
            return null;
        }
        
        foreach ($elements as $element) {
            $reportType = $element->getReportType()->collectValues()['ITEM']->getValue();
            $year = $sections[$element->getIblockSectionId()]->getUfYear();
            $result[$reportType][$year][] = [
                'NAME' => $element->getName()
            ];
        }

        return $result ?? null;
    }
}