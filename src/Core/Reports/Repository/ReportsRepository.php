<?php

declare(strict_types=1);

namespace Core\Reports\Repository;

use Bitrix\Iblock\Iblock;
use Bitrix\Iblock\Model\Section;
use Bitrix\Main\ORM\Objectify\Collection;
use Core\Internals\Struct\InstanceTrait;
use CUserFieldEnum;

class ReportsRepository
{
    use InstanceTrait;

    private const IBLOCK_ID = 332;
    private const PROPERTY_YEAR_ID = 752;

    private $elementsEntity;

    private $sectionsEntity;

    public function __construct()
    {
        $this->elementsEntity = Iblock::wakeUp(static::IBLOCK_ID)->getEntityDataClass();
        $this->sectionsEntity = Section::compileEntityByIblock(static::IBLOCK_ID);
    }

    /**
     * @return array|null
     */
    public function getElements(): ?Collection
    {
        $query = $this->elementsEntity::query();
        $rsElements = $query->setSelect(
            [
                'ID',
                'NAME',
                'REPORT_TYPE.ITEM',
                'IBLOCK_SECTION_ID'
            ]
        )
            ->setFilter(['=ACTIVE' => 'Y'])ä
            ->setOrder(['DATE_CREATE' => 'DESC'])
            ->setCacheTtl(3600)
            ->exec()
            ->fetchCollection();

        return $rsElements ?? null;
    }

    /**
     * @return array|null
     */
    public function getSections(): ?array
    {
        $sections = $this->sectionsEntity::getList([
            "filter" => [
                "ACTIVE" => "Y",
            ],
            "select" => ['ID', 'NAME', 'UF_YEAR'],
        ])->fetchCollection();

        return $this->resolveYears($sections) ?? null;
    }

    /**
     * @param Collection|null $sections
     *
     * @return array|null
     */
    private function resolveYears(?Collection $sections): ?array
    {
        $yearIds = [];

        foreach ($sections as $section) {
            $yearIds[] = $section->getUfYear();
        }

        if ($yearEnumValues = $this->getEnumUserPropValues(array_unique($yearIds))) {
            foreach ($sections as $section) {
                if ($yearValue = $yearEnumValues[$section->getUfYear()]) {
                    $result[(int)$section->getId()] = $section->setUfYear($yearValue);
                }
            }
        }

        return $result ?? [];
    }

    /**
     * @param array $valuesIds
     *
     * @return array|null
     */
    private function getEnumUserPropValues(array $valuesIds): ?array
    {
        if (!$valuesIds) {
            return null;
        }

        $filter['ID'] = $valuesIds;

        $rsEnum = CUserFieldEnum::GetList(["DEF" => "DESC", "SORT" => "ASC"], $filter);
        while ($enumProp = $rsEnum->fetch()) {
            $result[$enumProp['ID']] = $enumProp['VALUE'];
        }

        return $result ?? null;
    }
}