<?php

declare(strict_types=1);

use Core\Reports\Service\ReportsService;

class Reports extends CBitrixComponent
{
    /**
     * @return mixed|void|null
     */
    public function executeComponent()
    {
        $this->arResult['REPORTS'] = ReportsService::instance()->getReports();

        $this->includeComponentTemplate();
    }
}
