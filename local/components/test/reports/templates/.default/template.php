<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<div class="reports">
    <?php if ($arResult['REPORTS']): ?>
        <div class="tabs">
            <?php foreach (array_keys($arResult['REPORTS']) as $tabName): ?>
                <?php
                $showFirst = $showFirst ?: $tabName;
                ?>
                <div data-tab="<?= Cutil::translit($tabName, "ru") ?>"><?= $tabName ?></div>
            <?php endforeach; ?>
        </div>
        <div class="elements">ä
            <?php foreach ($arResult['REPORTS'] as $reportType => $elementGroup): ?>
                <div data-tab-el="<?= Cutil::translit($reportType, "ru") ?>" <?= ($showFirst !== $reportType ? 'style="display:none"' : '') ?>>
                    <?php foreach ($elementGroup as $year => $elements): ?>
                        <div class="year">
                            <?= $year ?>
                        </div>
                        <div class="year-elements">
                            <?php foreach ($elements as $element): ?>
                                <div><?= $element['NAME'] ?></div>
                            <?php endforeach; ?>
                        </div>

                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>

    <?php endif; ?>
</div>




