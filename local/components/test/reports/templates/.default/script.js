$(function () {
    $('.reports [data-tab]').click(function () {
        let tab = $(this).data('tab');
        $('.reports').find('[data-tab-el]').hide();
        $('.reports').find('[data-tab-el="'+ tab +'"]').show();
    });

    $('.year').click(function () {
        $(this).next('div').toggle();
    });
});